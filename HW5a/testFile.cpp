/*
Course: 600.120
Date: 04/01/2015
Assignment 5

Name: Oneeb Malik
Phone Number: 443-301-3153
Blackboard login: omalik2
Preferred Email: omalik2@jhu.edu

Name: Sienna Schmid
Phone Number: 240-405-2847
Blackboard Login: sschmi26
Preferred Email: sschmi26@jhu.edu
*/

#include <vector>
#include <iostream>
#include <cctype>
#include <map>
#include <list>
#include <cstdio>
#include <cstdlib>
#include "hw5.h"
#include <cassert>
#include <algorithm>

using namespace std;

void testWordCount(){

    vector<string> ourVector1 = {"I", "like", "cats"};
    vector<string>::size_type vsize;
    map<string, int> wordCount;
    map< string, list<string> > wordFollow;

    int sizeV = ourVector1.size();

    loadIntoMap(wordCount, wordFollow, sizeV, ourVector1);

    map<string, int>::iterator wi = wordCount.begin();

    assert(sizeV == 3);
    for(int i = 0; i < 3; i++) {
        assert(wi-> second == 1);
        wi++;
    }

    wordCount.clear();
    wordFollow.clear();

    vector<string> ourVector2;
    ourVector2.push_back("dogs");
    ourVector2.push_back("food");
    ourVector2.push_back("candy");
    ourVector2.push_back("food");
    ourVector2.push_back("food");
    ourVector2.push_back("dogs");
    ourVector2.push_back("party");
    ourVector2.push_back("food");
    sizeV = ourVector2.size();

    loadIntoMap(wordCount, wordFollow, sizeV, ourVector2);
    
    map<string, int>::iterator wi2 = wordCount.begin();    

    assert(sizeV == 8);
    assert(wi2-> first == "<START>");
    wi2++;
    assert(wi2-> first == "candy");
    assert(wi2-> second == 1); //candy
    wi2++;
    assert(wi2-> first == "dogs");
    assert(wi2-> second == 2); //dogs
    wi2++;
    assert(wi2-> first == "food");
    assert(wi2->second == 4); //food
    wi2++;
    assert(wi2-> first == "party");
    assert(wi2->second ==1); //party
    wi2++;
    
    wordCount.clear();
    wordFollow.clear();
    
}

void testWordFollow() {

    vector<string> ourVector1;;
    ourVector1.push_back("I");
    ourVector1.push_back("like");
    ourVector1.push_back("cats");
    vector<string>::size_type vsize;
    map<string, int> wordCount;
    map< string, list<string> > wordFollow;

    int sizeV = ourVector1.size();

    loadIntoMap(wordCount, wordFollow, sizeV, ourVector1);

    map<string, int>::iterator wi = wordCount.begin();

    map< string, list<string> >::iterator wli = wordFollow.begin();

    assert(wli->first.compare("<START>") == 0);
    assert(find((wli->second).begin(), (wli->second).end(), "I") != (wli->second).end());
    wli++;
    
    assert(wli->first.compare("I") == 0);
    assert(find((wli->second).begin(), (wli->second).end(), "like") != (wli->second).end());
    wli++;
    
    assert(wli->first.compare("cats") == 0);
    assert(find((wli->second).begin(), (wli->second).end(), "<END>") != (wli->second).end());
    wli++;

    assert(wli->first.compare("like") == 0);
    assert(find((wli->second).begin(), (wli->second).end(), "cats") != (wli->second).end());
    wli++;

    wordCount.clear();
    wordFollow.clear();

    vector<string> ourVector2;
    ourVector2.push_back("dogs");
    ourVector2.push_back("food");
    ourVector2.push_back("candy");
    ourVector2.push_back("food");
    ourVector2.push_back("food");
    ourVector2.push_back("dogs");
    ourVector2.push_back("party");
    ourVector2.push_back("food");
    sizeV = ourVector2.size();
    loadIntoMap(wordCount, wordFollow, sizeV, ourVector2);
   
    map< string, list<string> >::iterator wli2 = wordFollow.begin();

    list<string>::iterator newIt1 = wordFollow["dogs"].begin();
    list<string>::iterator newIt2 = wordFollow["food"].begin();

    assert(wli2->first.compare("<START>") == 0);
    assert(find((wli2->second).begin(), (wli2->second).end(), "dogs") != (wli2->second).end());
    wli2++;
    
    assert(wli2->first.compare("candy") == 0);
    assert(find((wli2->second).begin(), (wli2->second).end(), "food") != (wli2->second).end());
    wli2++;

    assert(wli2->first.compare("dogs") == 0);
    string toUse = *newIt1;
    assert((toUse.compare("party")) == 0);
    newIt1++;
    toUse = *newIt1;
    assert(toUse.compare("food") == 0);
    wli2++;

    newIt1 = wordFollow["food"].begin();
    toUse = *newIt1;
    assert(wli2->first.compare("food") == 0);
    assert(toUse.compare("<END>") == 0);
    newIt1++;
    toUse = *newIt1;
    assert(toUse.compare("dogs") == 0);
    newIt1++;
    toUse = *newIt1;
    assert(toUse.compare("food") == 0);
    newIt1++;
    toUse = *newIt1;
    assert(toUse.compare("candy") == 0);
    wli2++;

    assert(wli2->first.compare("party") == 0);
    assert(find((wli2->second).begin(), (wli2->second).end(), "food") != (wli2->second).end());
    wli2++;
}

void testRand(){
    vector<string> ourVector2;
    ourVector2.push_back("dogs");
    ourVector2.push_back("food");
    ourVector2.push_back("candy");
    ourVector2.push_back("food");
    ourVector2.push_back("food");
    ourVector2.push_back("dogs");
    ourVector2.push_back("party");
    ourVector2.push_back("food");
    int sizeV = ourVector2.size();
    map<string, int> wordCount;
    map< string, list<string> > wordFollow;
    loadIntoMap(wordCount, wordFollow, sizeV, ourVector2);	
    map< string, list<string> >::iterator wli2 = wordFollow.begin();
    map<string, int>::iterator wi = wordCount.begin();
    string outWord = wi->first;
    vector<string> newWords;

    int sizeOfV = randomizedVector(&newWords, wordFollow, 0, sizeV, &outWord, wi);
    assert(outWord.compare("dogs") == 0);
    sizeOfV = randomizedVector(&newWords, wordFollow, 1, sizeV, &outWord, wi);
    assert(outWord.compare("food") == 0);
    sizeOfV = randomizedVector(&newWords, wordFollow, 2, sizeV, &outWord, wi);
    assert(outWord.compare("food") == 0);
    sizeOfV = randomizedVector(&newWords, wordFollow, 1, sizeV, &outWord, wi);
    assert(outWord.compare("dogs") == 0);
    sizeOfV = randomizedVector(&newWords, wordFollow, 0, sizeV, &outWord, wi);
    assert(outWord.compare("party") == 0);
    sizeOfV = randomizedVector(&newWords, wordFollow, 0, sizeV, &outWord, wi);
    assert(outWord.compare("food") == 0);
    sizeOfV = randomizedVector(&newWords, wordFollow, 3, sizeV, &outWord, wi);
    assert(outWord.compare("candy") == 0);

    assert(newWords[0].compare("dogs") == 0);
    assert(newWords[1].compare("food") == 0);
    assert(newWords[2].compare("food") == 0);
    assert(newWords[3].compare("dogs") == 0);
    assert(newWords[4].compare("party") == 0);
    assert(newWords[5].compare("food") == 0);
    assert(newWords[6].compare("candy") == 0);
    sizeV = sizeOfV;
}


int main() {

    vector<string> newWords;

    printf("Running tests...\n");
    testWordCount();
    testWordFollow();
    testRand();    
   
    printf("All tests Passed!\n");

    return 0;
}





