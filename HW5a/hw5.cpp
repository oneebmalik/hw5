/*
Course: 600.120
Date: 04/01/2015
Assignment 5

Name: Oneeb Malik
Phone Number: 443-301-3153
Blackboard login: omalik2
Preferred Email: omalik2@jhu.edu

Name: Sienna Schmid
Phone Number: 240-405-2847
Blackboard Login: sschmi26
Preferred Email: sschmi26@jhu.edu
*/

#include <vector>
#include <iostream>
#include <cctype>
#include <map>
#include <list>
#include <cstdio>
#include <cstdlib>
#include "hw5.h"

using namespace std;

int loadIntoVector(vector<string> &wordVector) {

    string word;
    int size = 0;

    while(cin >> word) {
        wordVector.push_back(word);
        size++;
    }

    return size;   
}

void loadIntoMap(map<string, int> &wordCount, map< string, list<string> > &wordFollow, int numWords, const vector<string> &wordVector) {

    string word;
    string check;
    string checkNext;
    string wordNext;
    string temp;
    
    int num = 0;    
    int num2 = 0;

    ++wordCount["<START>"];
    
    while(num2 < numWords) {

        checkNext = wordVector[num2];

        if (num == 0) {
            wordFollow["<START>"].push_front(checkNext);
            check = checkNext;
            num++;
        } else {
            ++(wordCount[check]);
            wordFollow[check].push_front(checkNext);
            check = checkNext;
        }

        temp = checkNext;

        num2++;
    }

    ++(wordCount[temp]);
    wordFollow[temp].push_front("<END>");
}

int randomizedVector(vector<string> *newWords, map<string, list<string> > &wordFollow, int randIndex, int vSize, string *outWord, map<string, int>::iterator wi) {
    
    if(vSize == 0){
        *outWord = wi->first;
    }
    
    list<string>::iterator wordIt = wordFollow[*outWord].begin();

    for(int k = 0; k < randIndex; k++) {
        wordIt++;
    }

    *outWord = *wordIt;

    newWords->push_back(*outWord);

    vSize++;

    return vSize;
}

void printNewVector(vector<string> &newWords, int size) {
    
    for (int i = 0; i < size; i++) {

        if (newWords[i].compare("<END>") != 0) {
            cout << newWords[i] << " ";
        }
    }

    cout << endl;
}

    

