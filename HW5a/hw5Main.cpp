/*
Course: 600.120
Date: 04/01/2015
Assignment 5

Name: Oneeb Malik
Phone Number: 443-301-3153
Blackboard login: omalik2
Preferred Email: omalik2@jhu.edu

Name: Sienna Schmid
Phone Number: 240-405-2847
Blackboard Login: sschmi26
Preferred Email: sschmi26@jhu.edu
*/

#include "hw5.h"
#include <vector>
#include <iostream>
#include <cctype>
#include <map>
#include <list>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {

  srand(time(0));

  vector<string> wordVector;
  vector<string>::size_type vsize;
  map<string, int> wordCount;
  map< string, list<string> > wordFollow;
  vector<string> newWords;

  int sizeOfVector = loadIntoVector(wordVector);

  loadIntoMap(wordCount, wordFollow, sizeOfVector, wordVector);

  map<string, int>::iterator wi = wordCount.begin();
    
  int newVectorSize = 0;
  string outWord = wi->first;
  int randIndex1 = 0;

  do {
        
    int count = 0;

    for(list<string>::iterator wcs = wordFollow[outWord].begin(); wcs != wordFollow[outWord].end(); ++wcs) {
      count++;
    }

    randIndex1 = rand() % count;

    newVectorSize = randomizedVector(&newWords, wordFollow, randIndex1, newVectorSize, &outWord, wi);

  } while(!(outWord.compare("<END>") == 0));

  printNewVector(newWords, newVectorSize);

  return 0;

}
