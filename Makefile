# Makefile for HW5
#
# USAGE:
#
# // to compile:
# make
#
# // to compile tests and run tests:
# make test
#
#// to compile and create hw5Main driver executable (same thing as just 'make'):
# make hw5Main
#
# // remove compilation output files:
# make clean
#

# make variables let us avoid pasting these options in multiple places
CC = g++ 
#CXXFLAGS = -std=c++11  -Wall -Wextra -pedantic -O         # for final build
CXXFLAGS = -std=c++11 -Wall -Wextra -pedantic -O0 -ggdb  # for debugging
	
all: hw5.o hw5Main.o hw5Main 

test: testFile.o hw5.o testFile
	@echo "Running tests..."
	./testFile
	@echo "Tests successfully passed!"

testFile: testFile.o hw5.o
	$(CC) $(CXXFLAGS) -o testFile testFile.o hw5.o
testFile.o: testFile.cpp hw5.h
	$(CC) $(CXXFLAGS) -c testFile.cpp

hw5.o: hw5.cpp hw5.h
	$(CC) $(CXXFLAGS) -c hw5.cpp

hw5Main.o: hw5Main.cpp hw5.h
	$(CC) $(CXXFLAGS) -c hw5Main.cpp hw5.cpp
hw5Main: hw5Main.o hw5.o
	$(CC) $(CXXFLAGS) -o hw5 hw5Main.o hw5.o
	@echo "hw5 executable created"

clean:
	@rm -f *.o hw5 testFile hw5Main
	@echo "Executable files deleted"
