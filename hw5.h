/*
Course: 600.120
Date: 04/01/2015
Assignment 5

Name: Oneeb Malik
Phone Number: 443-301-3153
Blackboard login: omalik2
Preferred Email: omalik2@jhu.edu

Name: Sienna Schmid
Phone Number: 240-405-2847
Blackboard Login: sschmi26
Preferred Email: sschmi26@jhu.edu
*/

#include <vector>
#include <iostream>
#include <cctype>
#include <map>
#include <list>
#include <cstdio>
#include <cstdlib>

using namespace std;

//take input from stdin and load it into a vector of string
int loadIntoVector(vector<string> &wordVector);

//take a vector of strings and load into 2 different maps
void loadIntoMap(map<string, int> &wordCount, map< string, list<string> > &wordFollow, int numWords, const vector<string> &wordVector);

//take 2 maps and a random number to create a randomized vector of strings word by word
int randomizedVector(vector<string> *newWords, map<string, list<string> > &wordFollow, int randIndex, int vSize, string *outWord, map<string, int>::iterator wi);

//print out a vector of strings
void printNewVector(vector<string> &newWords, int size);
